from fastapi import FastAPI, Query
from transformers import pipeline

classifier = pipeline("sentiment-analysis")

app = FastAPI()

@app.get("/")
def hola():
    return {"Hello": "World"}

@app.get("/Prototipo 6")
def txt(phrase: str = Query(..., description="Frase para clasificar")):
    classification = classifier(phrase)
    return {"phrase": phrase, "classification": classification}

@app.get("/txt2")
def txt():
    text = "Hola profe"
    return text, classifier(text)